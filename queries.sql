CREATE TABLE dragon (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    color VARCHAR(128) NOT NULL, 
    wingspan NUMERIC(3,0) NOT NULL
);
CREATE TABLE egg (
    id INT PRIMARY KEY AUTO_INCREMENT,
    weight NUMERIC(3,0) NOT NULL,
    diameter NUMERIC(3,0) NOT NULL, 
    dragon_id VARCHAR(128) NOT NULL
);
ALTER TABLE egg ADD FOREIGN KEY (dragon_id) REFERENCES dragon (id);
CREATE TABLE embellishment (
    id INT PRIMARY KEY AUTO_INCREMENT,
    color VARCHAR(128) NOT NULL,
    pattern VARCHAR(128) NOT NULL,
    egg_id INT
);
ALTER TABLE egg ADD COLUMN embellishment_id INT;
ALTER TABLE egg ADD FOREIGN KEY (embellishment_id) REFERENCES embellishment (id);
ALTER TABLE embellishment ADD FOREIGN KEY (egg_id) REFERENCES egg (id);
CREATE TABLE land (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL
);
CREATE TABLE dragon_land (
    dragon_id INT NOT NULL,
    land_id INT NOT NULL
);
ALTER TABLE dragon_land ADD PRIMARY KEY (dragon_id, land_id);
ALTER TABLE dragon_land ADD FOREIGN KEY (dragon_id) REFERENCES dragon (id);
ALTER TABLE dragon_land ADD FOREIGN KEY (land_id) REFERENCES land (id);

INSERT INTO dragon (name, color, wingspan) VALUES("Dygir", "green", 200);
INSERT INTO dragon (name, color, wingspan) VALUES("Bondril", "red", 100);
INSERT INTO dragon (name, color, wingspan) VALUES("Onosse", "black", 250);
INSERT INTO dragon (name, color, wingspan) VALUES("Chiri", "yellow", 50);
INSERT INTO dragon (name, color, wingspan) VALUES("Lase", "blue", 300);

INSERT INTO egg (weight,diameter, dragon_id) VALUES(300,20, (SELECT id FROM dragon WHERE name = "Dygir"));
INSERT INTO egg (weight,diameter, dragon_id) VALUES(340,30, (SELECT id FROM dragon WHERE name = "Dygir"));
INSERT INTO egg (weight,diameter, dragon_id) VALUES(200,10, (SELECT id FROM dragon WHERE name = "Bondril"));
INSERT INTO egg (weight,diameter, dragon_id) VALUES(230,20, (SELECT id FROM dragon WHERE name = "Onosse"));
INSERT INTO egg (weight,diameter, dragon_id) VALUES(300,25, (SELECT id FROM dragon WHERE name = "Onosse"));
INSERT INTO egg (weight,diameter, dragon_id) VALUES(200,15, (SELECT id FROM dragon WHERE name = "Onosse"));

INSERT INTO embellishment (color, pattern, egg_id) VALUES("pink", "mesh",(SELECT id FROM egg WHERE weight = 300 AND diameter = 20));
INSERT INTO embellishment (color, pattern, egg_id) VALUES("black", "striped",(SELECT id FROM egg WHERE weight = 340 AND diameter = 30));
INSERT INTO embellishment (color, pattern, egg_id) VALUES("yellow", "dotted",(SELECT id FROM egg WHERE weight = 200 AND diameter = 10));
INSERT INTO embellishment (color, pattern, egg_id) VALUES("blue", "dotted",(SELECT id FROM egg WHERE weight = 230 AND diameter = 20));
INSERT INTO embellishment (color, pattern, egg_id) VALUES("green", "striped",(SELECT id FROM egg WHERE weight = 300 AND diameter = 25));
INSERT INTO embellishment (color, pattern, egg_id) VALUES("red", "mesh",(SELECT id FROM egg WHERE weight = 200 AND diameter = 15));

UPDATE egg SET embellishment_id = (SELECT id FROM embellishment WHERE color = "pink" AND pattern = "mesh") WHERE weight = 300 AND diameter = 20;
UPDATE egg SET embellishment_id = (SELECT id FROM embellishment WHERE color = "black" AND pattern = "striped") WHERE weight = 340 AND diameter = 30;
UPDATE egg SET embellishment_id = (SELECT id FROM embellishment WHERE color = "yellow" AND pattern = "dotted") WHERE weight = 200 AND diameter = 10;
UPDATE egg SET embellishment_id = (SELECT id FROM embellishment WHERE color = "blue" AND pattern = "dotted") WHERE weight = 230 AND diameter = 20;
UPDATE egg SET embellishment_id = (SELECT id FROM embellishment WHERE color = "green" AND pattern = "striped") WHERE weight = 300 AND diameter = 25;
UPDATE egg SET embellishment_id = (SELECT id FROM embellishment WHERE color = "red" AND pattern = "mesh") WHERE weight = 200 AND diameter = 15;

INSERT INTO land (name) VALUES ("Froze");
INSERT INTO land (name) VALUES ("Oswia");
INSERT INTO land (name) VALUES ("Oscyae");
INSERT INTO land (name) VALUES ("Oclurg");

INSERT INTO dragon_land (dragon_id, land_id) VALUES((SELECT id FROM dragon WHERE name = "Dygir"),(SELECT id FROM land WHERE name = "Froze"));
INSERT INTO dragon_land (dragon_id, land_id) VALUES((SELECT id FROM dragon WHERE name = "Bondril"),(SELECT id FROM land WHERE name = "Froze"));
INSERT INTO dragon_land (dragon_id, land_id) VALUES((SELECT id FROM dragon WHERE name = "Dygir"),(SELECT id FROM land WHERE name = "Oswia"));
INSERT INTO dragon_land (dragon_id, land_id) VALUES((SELECT id FROM dragon WHERE name = "Onosse"),(SELECT id FROM land WHERE name = "Oswia"));
INSERT INTO dragon_land (dragon_id, land_id) VALUES((SELECT id FROM dragon WHERE name = "Chiri"),(SELECT id FROM land WHERE name = "Oswia"));

CREATE VIEW eggs_details AS SELECT egg.diameter, egg.weight, embellishment.color, embellishment.pattern FROM egg INNER JOIN embellishment ON egg.id = embellishment.egg_id;
CREATE VIEW dragons_and_lands AS SELECT dragon.name AS "dragon name", land.name AS "land name" FROM dragon LEFT JOIN dragon_land ON dragon.id = dragon_land.dragon_id INNER JOIN land ON land.id = dragon_land.land_id;
CREATE VIEW dragon_eggs AS SELECT dragon.name, egg.weight, egg.diameter FROM dragon INNER JOIN egg ON egg.dragon_id = dragon.id;
CREATE VIEW dragons_without_eggs AS SELECT dragon.name FROM dragon LEFT JOIN egg ON dragon.id = egg.dragon_id WHERE egg.dragon_id IS NULL;

SELECT * FROM dragon;
SELECT * FROM egg;
SELECT * FROM embellishment;
SELECT * FROM land;
SELECT * FROM dragon_land;
SELECT * FROM eggs_details;
SELECT * FROM dragons_and_lands;
SELECT * FROM dragon_eggs;
SELECT * FROM dragons_without_eggs;

SELECT dragon.name FROM dragon WHERE dragon.wingspan >= 200 && dragon.wingspan <= 400;

DROP TABLE dragon_land;
DROP TABLE land;
ALTER TABLE embellishment DROP FOREIGN KEY embellishment_ibfk_1;
ALTER TABLE egg DROP FOREIGN KEY egg_ibfk_1;
DROP TABLE embellishment;
DROP TABLE egg;
DROP TABLE dragon;
DROP VIEW eggs_details;
DROP VIEW dragons_and_lands;
DROP VIEW dragon_eggs;
DROP VIEW dragons_without_eggs;